#!/bin/sh

for i in *.c; do
	echo "`basename $i .c` $i"	
	gcc -o `basename $i .c` $i -lpthread
done
